package oopsImplimentation;

public class HeallthyHamburger extends BasicHamburger {
	private final double egg = 15;
	private final double spinach = 10;
	private double heallthyHamburgerPrice;

	public HeallthyHamburger(String meat) {
		super("BrownRye", meat);
		this.heallthyHamburgerPrice = 0;

	}

	@Override
	public void setTopons(TopOns topons) {
		super.setTopons(topons);
	}

	public double getHeallthyHamburgerPrice() {
		return heallthyHamburgerPrice;
	}

	@Override
	public double totalToponPrice(String choice, int num) {
		double eggPrice = 0;
		double spinachPrice = 0;
		if (choice.toLowerCase().equals("egg") || choice.toLowerCase().equals("spinach")) {
			if (choice.toLowerCase().equals("egg")) {
				eggPrice = num*egg;
				this.heallthyHamburgerPrice += eggPrice;
				System.out.println("Added topon " + choice + " of requested quantity " + num
						+ " and the price added is " + eggPrice);
			}
			if (choice.toLowerCase().equals("spinach")) {
				spinachPrice = num*spinach;
				this.heallthyHamburgerPrice += spinachPrice;
				System.out.println("Added topon " + choice + " of requested quantity " + num
						+ " and the price added is " + spinachPrice);
			}
		} else {

			return super.totalToponPrice(choice, num);
		}
		return this.heallthyHamburgerPrice;

	}

	@Override
	public double totalHamburgerPrice() {
		if (getHeallthyHamburgerPrice() > 0) {
			System.out.println("Price for the Hamburger with " + getBreadtype() + " and " + getMeat() + " is "
					+ getBaseBuragerPrice());
			double hamburgerPrice = getBaseBuragerPrice() + getHeallthyHamburgerPrice();
			System.out.println("Total price of the HealthyHamburger is " + hamburgerPrice);
			return hamburgerPrice;
		}
		return super.totalHamburgerPrice();
	}

}
