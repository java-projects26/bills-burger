package oopsImplimentation;

public class TopOns {
	private final double lettuce = 10;
	private final double tomato = 10;
	private final double carrot = 12;
	private final double onion = 11;
	private double topOnPrice;

	public TopOns() {
		super();
		this.topOnPrice = 0;
	}

	public double getTopOnPrice() {
		return topOnPrice;
	}

	public double topOnPrice(String choice, int num) {
		double lettucePrice = 0;
		double tomatoPrice = 0;
		double carrotPrice = 0;
		double onionPrice = 0;

		if (choice.toLowerCase().equals("lettuce")) {
			lettucePrice = num * lettuce;
			this.topOnPrice += lettucePrice;
			System.out.println("Added topon " + choice + " of requested quantity " + num
					+ " and the price added is " + lettucePrice);
		}
		if (choice.toLowerCase().equals("tomato")) {
			tomatoPrice = num * tomato;
			this.topOnPrice += tomatoPrice;
			System.out.println("Added topon " + choice + " of requested quantity " + num
					+ " and the price added is " + tomatoPrice);
		}
		if (choice.toLowerCase().equals("carrot")) {
			carrotPrice = num * carrot;
			this.topOnPrice += carrotPrice;
			System.out.println("Added topon " + choice + " of requested quantity " + num
					+ " and the price added is " + carrotPrice);
		}
		if (choice.toLowerCase().equals("onion")) {
			onionPrice = num * onion;
			this.topOnPrice += onionPrice;
			System.out.println("Added topon " + choice + " of requested quantity " + num
					+ " and the price added is " + onionPrice);
		}

		return this.topOnPrice;
	}

}
