package oopsImplimentation;

public class Main {

	public static void main(String[] args) {
		double basicPrice = 0;
		double hamburgerTotalPrice = 0;
		// Basic Hamburger test
		System.out.println("////Basic Hamburger test///");
		BasicHamburger basicHamburger = new BasicHamburger("EnglighMuffin", "chicken");
		basicHamburger.setTopons(new TopOns());// basicPrice = basicHamburger.totalHamburgerPrice();
		hamburgerTotalPrice = basicPrice + basicHamburger.totalToponPrice("lettuce", 1)
				+ basicHamburger.totalToponPrice("tomato", 2);
		basicPrice = basicHamburger.totalHamburgerPrice();
		System.out.println();

		// Healthy Hamburger test
		System.out.println("///Healthy Hamburger test///");
		HeallthyHamburger healthyHamburger = new HeallthyHamburger("MuttonSlice");
		healthyHamburger.setTopons(new TopOns());
		// basicPrice = healthyHamburger.totalHamburgerPrice();
		hamburgerTotalPrice = basicPrice + healthyHamburger.totalToponPrice("egg", 2)
				+ healthyHamburger.totalToponPrice("spinach", 1);
		basicPrice = healthyHamburger.totalHamburgerPrice();
		System.out.println();

		// Deluxe Hamburger test
		System.out.println("////Deluxe Hamburger test///");
		basicHamburger.setTopons(new TopOns());
		basicHamburger.setName("deluxe");
		// basicPrice = basicHamburger.totalHamburgerPrice();
		hamburgerTotalPrice = basicPrice + basicHamburger.totalToponPrice("lettuce", 1)
				+ basicHamburger.totalToponPrice("tomato", 2);
		basicPrice = basicHamburger.totalHamburgerPrice();
	}

}
