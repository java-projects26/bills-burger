package oopsImplimentation;

public class BasicHamburger {
	private String name;
	private String breadtype;
	private String meat;
	private TopOns topons;
	private final double baseBuragerPrice = 25;
	private double price;
	private double topOnPrice;

	public BasicHamburger(String breadtype, String meat) {
		super();
		this.breadtype = breadtype;
		this.meat = meat;
		this.price = 0;
		this.name = "non-deluxe";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMeat() {
		return meat;
	}

	public double getBaseBuragerPrice() {
		return baseBuragerPrice;
	}

	public void setTopons(TopOns topons) {
		this.topons = topons;
	}

	public String getBreadtype() {
		return breadtype;
	}

	public TopOns getTopons() {
		return topons;
	}

	public double getPrice() {
		return price;
	}

	public double totalToponPrice(String choice, int num) {
		topOnPrice = topons.topOnPrice(choice, num);
		return topOnPrice;
	}

	public double totalHamburgerPrice() {
		System.out.println(
				"Price for the Hamburger with " + this.breadtype + " and " + this.meat + " is " + baseBuragerPrice);
		double hamburgerPrice = baseBuragerPrice + topons.getTopOnPrice();
		System.out.println("Total price of the Hamburger is " + hamburgerPrice);
		if (getName().toLowerCase().equals("deluxe")) {
			System.out.println("Cost of Chips and dips is 20");
			hamburgerPrice += 20;
			System.out.println("Total price of the DeluxeHamburger is " + hamburgerPrice);
		}

		return hamburgerPrice;
	}

}
